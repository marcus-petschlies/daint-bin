#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')

echo "# [$MyName] (`date`)"

input_file=$MyName.in
Conf=( )

work_path=
exec_dir=

source_coords_filename=
source_coords_select=( 1 0 )
n_coherent_source=1

myemail=

exec_name=
exec_options=

with_submit=no
LL=
TT=
ensemble_name=

invcon_production=yes

walltime_prod=12:00:00
tasks_per_core=1
nodes_per_call=4
tasks_per_node=1
cpus_per_task=12
partition=normal
gpus_per_node=1
modules=
account=s982
constraint=gpu
exclude="nid0[6972-7035]"

quda_vars=yes

run_prefix=""

QUDA_ENABLE_GDR=0
QUDA_ENABLE_P2P=0
QUDA_COMMIT_HASH=
QUDA_ENABLE_DEVICE_MEMORY_POOL=0
QUDA_ENABLE_DSLASH_COARSE_POLICY=0

seed_offset=RANDOM

answer_wait_time=1
sleep_seconds=3
mail_type="FAIL"

smiss_file=""
sourceid=0
sourceid2=-1
sourceid_step=1

conf_prefix=

if [ $# -eq 0 ]; then
  echo -e "# [$MyName] WARNING: no command line arguments provided, keeping default values\n"
else
  while [ "$1" ]; do
    case "$1" in
      "-x") with_submit=$2; shift 2;;
      "-d") work_path=$2; shift 2;;
      "-i") input_file=$2; shift 2;;
      *) exit 1;;
    esac
  done
fi

if ! [ -e ${input_file} ]; then
  echo "[$MyName] Error, could not find input file ${input_file}"
  exit 1
else
  echo "# [$MyName] using input file $input_file"
fi
source $input_file

if [ "X$run_prefix" == "X" ]; then
  echo "[$MyName] Error, need a run prefix"
  exit 1
fi

if [ "X$LL" == "X" ] || [ "X$TT" == "X" ] ; then
  echo "# [$MyName] Error, need L and T"
  exit 1
fi

if [ "X$ensemble_name" == "X" ] ; then
  echo "# [$MyName] Error, need ensemble name"
  exit 1
fi

if [ "X$source_coords_filename" == "X" ] ; then
  echo "# [$MyName] Warning, no source coords filename"
#  exit 1
fi

if [ "X$nodes_per_call" == "X" ] || [ "X$tasks_per_node" == "X" ] || [ "X$cpus_per_task" == "X" ] ; then
  echo "# [$MyName] Error, number of nodes_per_call, tasks_per_node, cpus_per_task"
  exit 1
fi

if [ "X$exec_dir" == "X" ] || [ "X$work_path" == "X" ]; then
  echo "# [$MyName] Error, need exec_dir and work_path"
  exit 1
fi

if [ "X$exec_name" == "X" ]; then
  echo "# [$MyName] Error, need exec_name"
  exit 1
fi

#if [ "X$gpus_per_node" == "X" ]; then
#  echo "# [$MyName] Error, need gpus_per_node and gpu_type"
#  exit 1
#fi

if [ "X$partition" == "X" ]; then
  echo "# [$MyName] Error, need partition"
  exit 1
fi

if [ "X$myemail" == "X" ]; then
  echo "# [$MyName] Error, need myemail"
  exit 1
fi

         job_mask=$work_path/job.sh
   cvc_input_mask=$work_path/${run_prefix}.input
tmLQCD_input_mask=$work_path/invert.input

nconf=$(echo ${Conf[*]} | wc -w)

if [ $tasks_per_node -ne $gpus_per_node ]; then
  echo "# [$MyName] resetting tasks_per_node <- gpus_per_node"
  tasks_per_node=$gpus_per_node
fi

JOB_LOG=$work_path/JOB_LOG

ntasks=$(( $nodes_per_call * $tasks_per_node ))

cat << EOF
###########################################################
###########################################################
##                                                       ##
## [$MyName] did you check the input files?   ##
##                                                       ##
###########################################################
###########################################################
#
# [$MyName] with_submit            = $with_submit
# [$MyName] ensemble name          = $ensemble_name
# [$MyName] work_path              = $work_path
# [$MyName]
# [$MyName] exec_dir               = $exec_dir
# [$MyName] exec_name              = $exec_name
# [$MyName] exec_options           = $exec_options
# [$MyName]
# [$MyName] L                      = $LL
# [$MyName] T                      = $TT
# [$MyName]
# [$MyName] source coords file     = $source_coords_filename
# [$MyName] source coords select   = ${source_coords_select[0]}, ${source_coords_select[1]}
# [$MyName]
# [$MyName] job mask               = $job_mask
# [$MyName]
# [$MyName] input mask             = $cvc_input_mask
# [$MyName] tmLQCD_input_mask      = $tmLQCD_input_mask
# [$MyName]
# [$MyName] Conf                   = ${Conf[*]}
# [$MyName]
# [$MyName] walltime_prod          = $walltime_prod
# [$MyName]
# [$MyName] ntasks                 = $ntasks
# [$MyName] nodes_per_call         = $nodes_per_call
# [$MyName] tasks_per_node         = $tasks_per_node
# [$MyName] cpus_per_task          = $cpus_per_task
# [$MyName]
# [$MyName] gpus_per_node          = $gpus_per_node
# [$MyName] gpu_type               = $gpu_type
# [$MyName] quda_resource_path     = $quda_resource_path
# [$MyName]
# [$MyName] myemail                = $myemail
# [$MyName] mail_type              = $mail_type
# [$MyName]
# [$MyName] modules                = $modules
# [$MyName]
# [$MyName] quda_vars              = $quda_vars
# [$MyName]
# [$MyName] seed_offset            = $seed_offset
# [$MyName]
# [$MyName] smiss_file             = $smiss_file
# [$MyName]


EOF

sleep ${answer_wait_time}s
echo "# [$MyName] Okay ? (yes)"
read answer
if [ "X$answer" != "Xyes" ]; then
  echo "[$MyName] abort"
  exit 1
else
  echo "# [$MyName] continue"
fi

###########################################################
# inversion+contraction job
###########################################################
if [ "X$invcon_production" == "Xyes" ]; then

  for g in ${Conf[*]}; do

    date_tag=`date +%Y_%m_%d_%H_%M_%S`

    gfmt=$(printf "%.4d" $g)

    WDIR=$work_path/$g
    mkdir -p $WDIR 
    cd $WDIR

    job_tag=${g}.${date_tag}
    job=${run_prefix}.${job_tag}.sh

    cvc_input=${run_prefix}.${job_tag}.input
    tmLQCD_input=invert.input

    if ! [ -e ${conf_prefix}.${gfmt} ] 
    then 
      # echo "[$MyName] Cannot find config file ${conf_prefix}.${gfmt} ; skip"
      # continue
      echo "[$MyName] Warning, cannot find config file ${conf_prefix}.${gfmt}"
    fi


cat << EOF
# [$MyName] gauge config           = $g
# [$MyName] work_dir               = $PWD
# [$MyName]
# [$MyName] date_tag               = $date_tag
# [$MyName] job_tag                = $job_tag
# [$MyName] job                    = $job
# [$MyName] cvc_input              = $cvc_input
# [$MyName] tmLQCD_input           = $tmLQCD_input
# [$MyName] conf_prefix            = $conf_prefix
EOF


    if [ "X${smiss_file}X" !=  "XX" ]; then
      s=($(awk '$1=='$g' {print $2}' $smiss_file | sort -un )) 
      sourceid=$(echo ${s[*]} | tr ' ' '\n' | head -n 1)
      sourceid2=$(echo ${s[*]} | tr ' ' '\n' | tail -n 1)
cat << EOF
# [$MyName]
# [$MyName] sourceid               = $sourceid
# [$MyName] sourceid2              = $sourceid2
EOF
    fi
   
cat << EOF

EOF

### export CUDA_MAX_DEVICE_CONNECTIONS=1



cat << EOF > $job 
#!/bin/bash -l
#SBATCH --job-name=${ensemble_name}_${run_prefix}_$job_tag
#SBATCH --mail-type=${mail_type}
#SBATCH --mail-user=${myemail}
#SBATCH --nodes=${nodes_per_call}
#SBATCH --ntasks=${ntasks}
#SBATCH --ntasks-per-core=${tasks_per_core}
#SBATCH --ntasks-per-node=${tasks_per_node}
#SBATCH --cpus-per-task=${cpus_per_task}
#SBATCH --time=${walltime_prod}
#SBATCH --partition=${partition}
#SBATCH --account=${account}
#SBATCH --constraint=${constraint}
#SBATCH --contiguous
EOF

if [ "X${exclude}X" != "XX" ]; then
cat << EOF >> $job 
#SBATCH --exclude=${exclude}
EOF
fi

cat << EOF >> $job 

$modules

EOF

if [ "X${dynamic_lib_path}X" != "XX" ]; then
cat << EOF >> $job 
export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:${dynamic_lib_path}

EOF

fi

if [ "X${quda_vars}X" == "XyesX" ]; then
  my_quda_resource_path=
  if [ "X${quda_resource_path}X" != "XX" ]; then
    if [ -d ${quda_resource_path} ]; then
      my_quda_resource_path="$WDIR/quda-tune-cache"
      mkdir -p $my_quda_resource_path
      cp -av ${quda_resource_path}/* ${my_quda_resource_path}/
    else
      my_quda_resource_path="$WDIR/quda-\$SLURM_JOB_ID"
    fi
  else
    my_quda_resource_path="$WDIR/quda-\$SLURM_JOB_ID"
  fi
  echo "# [$MyName] my_quda_resource_path = $my_quda_resource_path"

cat << EOF >> $job      
export QUDA_RESOURCE_PATH="${my_quda_resource_path}"
if [ "X\${QUDA_RESOURCE_PATH}X" != "XX" ]; then
  mkdir -p \$QUDA_RESOURCE_PATH
fi

export CRAY_CUDA_MPS=1
export QUDA_ENABLE_DEVICE_MEMORY_POOL=${QUDA_ENABLE_DEVICE_MEMORY_POOL}
export QUDA_ENABLE_DSLASH_COARSE_POLICY=${QUDA_ENABLE_DSLASH_COARSE_POLICY}
export QUDA_ENABLE_GDR=${QUDA_ENABLE_GDR}
export QUDA_ENABLE_P2P=${QUDA_ENABLE_P2P}
export QUDA_ENABLE_TUNING=1
export MPICH_RDMA_ENABLED_CUDA=1
export MPICH_NEMESIS_ASYNC_PROGRESS=0

export QUDA_REORDER_LOCATION=GPU

export GOMP_CPU_AFFINITY="0 1 2 3 4 5 6 7 8 9 10 11"

EOF
fi

cat << EOF >> $job 
export OMP_NUM_THREADS=${cpus_per_task}

out=out.${date_tag}.\$SLURM_JOB_ID
err=err.${date_tag}.\$SLURM_JOB_ID

workdir=${WDIR}

mkdir -p \$workdir
cd \$workdir

exec_dir=${exec_dir}
exec_name=${exec_name}
exec_options="${exec_options}"

input=${cvc_input}

echo "# [\$SLURM_JOB_ID] \`date\` " > \${out}
srun \${exec_dir}/\${exec_name} \${exec_options} -f \$input 1>>\$out 2>\$err
echo "# [\$SLURM_JOB_ID] \`date\` " >> \${out}

exit 0
EOF
### srun -n \${SLURM_NTASKS} --ntasks-per-node=\${SLURM_NTASKS_PER_NODE} -c \${SLURM_CPUS_PER_TASK} -C gpu \${exec_dir}/\${exec_name} \${exec_options} -f \$input 1>\$out 2>\$err

    source_coords_list=
    source_coords_number=0
    if [ "X${source_coords_filename}X" != "XX" ]; then
      if [ "X${stream}X" != "XX" ]; then
        source_coords_list=($(awk '
            $1 == "'$stream'" && $2=='$g' && $3<'$TT'/'$n_coherent_source' {
              printf("%d,%d,%d,%d\n", $3,$4,$5,$6)
            }' $source_coords_filename | awk '(NR-1) % '${source_coords_select[0]}' == '${source_coords_select[1]}' '))
      else
        source_coords_list=($(awk '
            $1=='$g' && $2<'$TT'/'$n_coherent_source' {
              printf("%d,%d,%d,%d\n", $2,$3,$4,$5)
            }' $source_coords_filename | awk '(NR-1) % '${source_coords_select[0]}' == '${source_coords_select[1]}' '))
      fi
      source_coords_number=$(echo ${source_coords_list[*]} | wc -w)
      echo "# [$MyName] source coords number = $source_coords_number"
    fi

    seed=$g
    if [ "X${seed_offset}X" == "XRANDOMX" ]; then
      seed=$(( $seed + $RANDOM  ))
    else
      seed=$(( $seed + $seed_offset ))
    fi
    echo "# [$MyName] seed = $seed"

    # cvc input file
    cat $cvc_input_mask | awk '
      /^Nconf[\ =]/           { print "Nconf = '${g}'"; next}
      /^seed[\ =]/            { print "seed = "('${seed}'); next}
      /^num_threads[\ =]/     { print "num_threads = '${cpus_per_task}'"; next}
      /^sourceid[\ =]/        { print "sourceid  = ",'${sourceid}'; next}
      /^sourceid2[\ =]/       { print "sourceid2 = ",'${sourceid2}'; next}
      /^sourceid_step[\ =]/       { print "sourceid_step = ",'${sourceid_step}'; next}
      {print}' > $WDIR/$cvc_input

    # append source coords list
    for (( isrc=0; isrc<$source_coords_number; isrc++)); 
    do
      printf "source_coords = %s\n" ${source_coords_list[$isrc]}
    done >> $WDIR/$cvc_input

    if [ -e $tmLQCD_input_mask ]; then
      cat $tmLQCD_input_mask | awk '
        /^InitialStoreCounter[\ =]/  {print "InitialStoreCounter = '${g}'"; next}
        /^initialstorecounter[\ =]/  {print "initialstorecounter = '${g}'"; next}
        /^ompnumthreads[\ =]/        {print "ompnumthreads = '${cpus_per_task}'"; next}
        /^GaugeConfigInputFile[\ =]/ {print "GaugeConfigInputFile= '${conf_prefix}'"; next}
        {print}' > $WDIR/$tmLQCD_input
    fi

    # submit the job
    if [ "X$with_submit" == "Xyes" ]; then
      echo "# [$MyName] (`date`) $job_tag " 2>&1 | tee -a $JOB_LOG
      sbatch $job 2>&1 | tee -a $JOB_LOG
    fi

    sleep ${sleep_seconds}s
 
  done  # end of loop on configurations

fi  # of if invcon job

echo "# [$MyName] (`date`)"
exit 0
