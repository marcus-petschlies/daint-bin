#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh$/,"",$NF);print $NF}')

kmax=-1
kmin=0
ksqrmax=-1
ksqrmin=0
name=""

while [ "$1" ]; do
  case "$1" in
    "-k")
      kmin=$2
      shift 2;;
    "-K")
      kmax=$2
      shift 2;;
    "-K2")
      ksqrmax=$2
      shift 2;;
    "-k2")
      ksqrmin=$2
      shift 2;;
    "-n")
      name=$2
      shift 2;;
    *)
      exit 1;;
  esac
done

if [ -e $MyName.in ]; then
  source $MyName.in
fi

if [ "X$name" == "X" ]; then
  echo "[$MyName] Error, need name"
  exit 1
fi
 
cat << EOF 
# [$MyName] `date`
# [$MyName] kmin    = $kmin
# [$MyName] kmax    = $kmax
# [$MyName] ksqrmax = $ksqrmax
# [$MyName] ksqrmin = $ksqrmin
# [$MyName]

EOF


for((kx=$kmin; kx<=$kmax; kx++)); do
for((ky=$kmin; ky<=$kmax; ky++)); do
for((kz=$kmin; kz<=$kmax; kz++)); do
  for sx in -1  1; do
  for sy in -1  1; do
  for sz in -1  1; do
    p=( $(( $kx * $sx)) $(( $ky * $sy)) $(( $kz * $sz)) )

    p2=$(( ${p[0]}* ${p[0]} + ${p[1]}* ${p[1]} + ${p[2]}* ${p[2]} ))
    if [ $p2 -gt $ksqrmax ] || [ $p2 -lt $ksqrmin ]; then
      continue
    fi

    printf "%s = %d,%d,%d\n" $name ${p[*]}

  done
  done
  done
done
done
done | sort -u > .${MyName}.lst

count=$( cat .${MyName}.lst | wc -l )
cat  .${MyName}.lst

echo "# [$MyName] number of momenta = $count"
echo "# [$MyName] (`date`) all done"

exit 0
