#!/bin/bash -l
#SBATCH --job-name="cA211a.30.32_exdefl_1020_light"
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=marcus.petschlies@hiskp.uni-bonn.de
#SBATCH --nodes=4
#SBATCH --ntasks=4
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=12
#SBATCH --constraint=gpu
#SBATCH --time=02:00:00
#SBATCH --exclude=nid0[6972-7035]
#SBATCH --contiguous
#SBATCH --partition=normal
#SBATCH --account=s849

### #SBATCH --output=
### #SBATCH --error=
# -----------------------------------------------------------------
# THINGS THAT ARE SET BY PROCESSING SCRIPT, SPECIFIC TO EACH CONFIG
# -----------------------------------------------------------------

TRAJ=1024

MYLOCALDIR="/scratch/snx3000/mpetschl/cA211a.30.32/exdefl"
mkdir -p $MYLOCALDIR && cd $MYLOCALDIR || exit 1
CONF_DIR="/scratch/snx3000/mpetschl/cA211a.30.32/configs"
CONF_FILE="conf.1020"
CONF_FULL="${CONF_DIR}/${CONF_FILE}"

MUL="0.0030"  # fix for light loops or strange loop
MASSLABEL="light"

THESEED="$(( ${SLURM_JOB_ID} / ${TRAJ} ))"
echo "# [$SLURM_JOB_ID] THESEED = $THESEED "

# loop save filename
RUN_TYPE=probD8_part1  
SCRATCH_DIR=${MYLOCALDIR}/${TRAJ}
mkdir -p $SCRATCH_DIR
LOOP_DIR=${SCRATCH_DIR}
mkdir -p $LOOP_DIR && cd $LOOP_DIR || exit 2
LOOP_FILENAME=${LOOP_DIR}/loop_${MASSLABEL}quark_conf_${CONF_FILE}_runtype_${RUN_TYPE}

# max momentum in the loop
QSQ_MAX=1

# -----------------------------------------------------
# THINGS THAT DO/MIGHT NEED TO CHANGE FOR EACH ENSEMBLE
# -----------------------------------------------------

# Ensemble specific!
KAPPA=0.1400645
CSW=1.74
INV_TOL=1.0e-9

# modules
source /users/mpetschl/modules.gnu.plugin

# QUDA tune cache location
MYQUDADIR="${MYLOCALDIR}/quda-tune-${SLURM_JOB_ID}"
### MYQUDADIR="${MYLOCALDIR}/quda-tune"
mkdir -p $MYQUDADIR
# executable
### QUDA_BIN="/project/s849/mpetschl/software/ETMC-QUDA/quda-QKXTM-Multigrid-PlugIn/build/qkxtm/Calc_Loops"
### QUDA_BIN="/project/s849/gasbarro/cypruscode/executable/Calc_Loops"
### QUDA_BIN="/project/s849/gasbarro/cypruscode/executable/build_300419/Calc_Loops"
QUDA_BIN="/project/s849/mpetschl/software/ETMC-QUDA/quda-QKXTM-Multigrid-PlugIn/build-adg/qkxtm/Calc_Loops"


#lattice size
L=32
T=64

# Determine number of nodes, gridsizes and dims
GPUPERNODE=1
NNODES=4
XGRID=1
YGRID=1
ZGRID=1
TGRID=4

# number of stochastic sources
NSTOCH=128
NDUMPSTEP=1

#
# DONT FORGET TO CHECK THAT YOU SET THE MG PARAMETERS PROPERLY BELOW

# -----------------------------------------------------
# THINGS THAT YOU DON"T HAVE TO CHANGE, BELOW THIS LINE
# -----------------------------------------------------
cat << EOF
# [$SLURM_JOB_ID] Report: Slurm Configuration
# [$SLURM_JOB_ID] Job ID: ${SLURM_JOBID}
# [$SLURM_JOB_ID] Node list: ${SLURM_JOB_NODELIST}
# [$SLURM_JOB_ID] Node cabinets and electric groups
EOF
scontrol show nodes ${SLURM_JOB_NODELIST} | grep -i activefeatures | sort -u

QUDA_DIR=${MYQUDADIR}
mkdir -p ${QUDA_DIR}
NUM_THREADS=12

export CRAY_CUDA_MPS=0 
#source /users/krikitos/sources_to_load/loadModulesGnu_v2.sh
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/project/s849/mpetschl/software/OPENBLAS/OpenBLAS/

export QUDA_RESOURCE_PATH=${QUDA_DIR}
export OMP_NUM_THREADS=${NUM_THREADS}
#export KMP_AFFINITY="granularity=fine,proclist=[0,1,2,3,4,5,6,7,8,9,10,11],explicit, verbose"
export GOMP_CPU_AFFINITY="0 1 2 3 4 5 6 7 8 9 10 11"
export CUDA_MAX_DEVICE_CONNECTIONS=1
ulimit -c 0
echo " "

export QUDA_ENABLE_DEVICE_MEMORY_POOL=0
export QUDA_ENABLE_DSLASH_COARSE_POLICY=0


#####################################
#####################################
##
## Operator & Solver options
##
#####################################
#####################################
DSLASH_TYPE=twisted-clover

# mg vs cg inverter
INV_TYPE="gcr" #for mg
SOLVE_TYPE=direct-pc #for mg
#INV_TYPE="cg"
#SOLVE_TYPE=normop-pc #for cg 

MASS_NORM=mass
PIPELINE=0
NGCRKRYLOV=24
NITER=1000
VERIFY=false

PREC=double
PREC_SLOPPY=single
PREC_PRECON=single
PREC_NULL=half
RECON=12
RECON_SLOPPY=12
RECON_PRECON=8

#####################################
#####################################
##
##  ARPACK Options
##
#####################################
#####################################
PolyDeg=300
nEv=20
nKv=40
alphaARPACK=a1p0em3
betaARPACK=b4p00
spectrumPart=SR
isACC=true
tolARPACK=1.0e-8
maxIterARPACK=100000
aminARPACK=$(echo $alphaARPACK | sed s@a@@ | sed s@p@\\.@ | sed s@em@\\e-@)
amaxARPACK=$(echo $betaARPACK | sed s@b@@|sed s@p@\\.@)
UseFullOp=true
UseEven=false

#####################################
#####################################
##
## Loop options
##
#####################################
#####################################
SOURCE_TYPE=random
#K_PROB=3
K_PROB=0
#SC_DIL=true
SC_DIL=false
NC_LOW=0
NC_HIGH=256

VERBOSITY_LEVEL=summarize

XDIM=$((L/XGRID))
YDIM=$((L/YGRID))
ZDIM=$((L/ZGRID))
TDIM=$((T/TGRID))

LOOP_FILE_FORMAT=HDF5

#####################################
#####################################
##
## MG options
##
#####################################
#####################################
MG_NU_PRE=0
MG_NU_POST=4
MG_SETUP_TOL="5e-7"
MG_SETUP_ITER_0="0 1"
MG_SETUP_ITER_1="1 1"
MG_OMEGA=0.85
MG_SETUP_TYPE='null'
MG_PRE_ORTH=false
MG_POST_ORTH=true
MG_VERBOSITY=silent

MG_LEVELS=3
#Add more here for >2 levels
MG_N_VEC_0="0 24"
MG_BLK_SZE_0="0 4 4 4 4"
MG_MU_FACTOR_0="1 1.0"

MG_N_VEC_1="1 24"
MG_BLK_SZE_1="1 2 2 2 2"
MG_MU_FACTOR_1="2 14.0"

MG_COARSE_SOLVER_1='1 gcr'
MG_COARSE_TOL_1='1 0.2'
MG_COARSE_MAXITER_1='1 70'

MG_COARSE_SOLVER_2='2 gcr'
MG_COARSE_TOL_2='2 0.3'
MG_COARSE_MAXITER_2='2 70'

echo "GRID(X,Y,Z,T) = ${XGRID} , ${YGRID} , ${ZGRID} , ${TGRID}"
echo "DIM(X,Y,Z,T)  = ${XDIM} , ${YDIM} , ${ZDIM} , ${TDIM}"
echo " "
pathArpackLogfile=${SCRATCH_DIR}/arpack_log_${RUN_TYPE}_fullOp_NeV${nEv}_NkV${nKv}_Deg${PolyDeg}_${alphaARPACK}_${betaARPACK}.${TRAJ}.log

#-c ${NUM_THREADS}

#####################################
#####################################
##
## program call
##
#####################################
#####################################

RUN_COMMAND="srun -n ${NNODES}
--ntasks-per-node=${GPUPERNODE} -c ${NUM_THREADS} -C gpu ${QUDA_BIN} \
--inv-type ${INV_TYPE} \
--solve-type ${SOLVE_TYPE} \
--dslash-type ${DSLASH_TYPE} \
--prec ${PREC} \
--prec-sloppy ${PREC_SLOPPY} \
--prec-precondition ${PREC_PRECON} \
--recon ${RECON} \
--recon-sloppy ${RECON_SLOPPY} \
--recon-precondition ${RECON_PRECON} \
--xdim ${XDIM} \
--ydim ${YDIM} \
--zdim ${ZDIM} \
--tdim ${TDIM} \
--xgridsize ${XGRID} \
--ygridsize ${YGRID} \
--zgridsize ${ZGRID} \
--tgridsize ${TGRID} \
--load-gauge ${CONF_FULL} \
--traj ${TRAJ} \
--kappa ${KAPPA} \
--mu ${MUL} \
--csw ${CSW} \
--Q-sqMax ${QSQ_MAX} \
--loop-filename ${LOOP_FILENAME} \
--Nstoch ${NSTOCH} \
--seed ${THESEED} \
--NdumpStep ${NDUMPSTEP} \
--PolyDeg ${PolyDeg} \
--nEv ${nEv} \
--nKv ${nKv} \
--spectrumPart ${spectrumPart} \
--isACC ${isACC} \
--tolARPACK ${tolARPACK} \
--maxIterARPACK ${maxIterARPACK} \
--pathArpackLogfile ${pathArpackLogfile} \
--aminARPACK ${aminARPACK} \
--amaxARPACK ${amaxARPACK} \
--useFullOp ${UseFullOp} \
--useEven ${UseEven} \
--source-type ${SOURCE_TYPE} \
--k-probing ${K_PROB} \
--hadamLow ${NC_LOW} \
--hadamHigh ${NC_HIGH} \
--spinColorDil ${SC_DIL} \
--useFullOp ${UseFullOp} \
--useEven ${UseEven} \
--loop-file-format ${LOOP_FILE_FORMAT} \
--verbosity-level ${VERBOSITY_LEVEL} \
--mass-normalization ${MASS_NORM} \
--pipeline ${PIPELINE} \
--ngcrkrylov ${NGCRKRYLOV} \
--niter ${NITER} \
--verify ${VERIFY} \
--tol ${INV_TOL} \
--mg-levels ${MG_LEVELS} \
--mg-nvec ${MG_N_VEC_0} \
--mg-nvec ${MG_N_VEC_1} \
--mg-block-size ${MG_BLK_SZE_0} \
--mg-block-size ${MG_BLK_SZE_1} \
--mg-nu-pre ${MG_NU_PRE} \
--mg-nu-post ${MG_NU_POST} \
--mg-setup-tol ${MG_SETUP_TOL} \
--mg-setup-inv 0 cg \
--mg-setup-inv 1 cg \
--mg-mu-factor ${MG_MU_FACTOR_0} \
--mg-mu-factor ${MG_MU_FACTOR_1} \
--mg-omega ${MG_OMEGA} \
--mg-setup-iters ${MG_SETUP_ITER_0} \
--mg-setup-iters ${MG_SETUP_ITER_1} \
--mg-setup-type ${MG_SETUP_TYPE} \
--mg-pre-orth ${MG_PRE_ORTH} \
--mg-post-orth ${MG_POST_ORTH} \
--mg-verbosity 0 ${MG_VERBOSITY} \
--mg-verbosity 1 ${MG_VERBOSITY} \
--mg-coarse-solver ${MG_COARSE_SOLVER_1} \
--mg-coarse-solver-tol ${MG_COARSE_TOL_1} \
--mg-coarse-solver-maxiter ${MG_COARSE_MAXITER_1}
--mg-coarse-solver ${MG_COARSE_SOLVER_2} \
--mg-coarse-solver-tol ${MG_COARSE_TOL_2} \
--mg-coarse-solver-maxiter ${MG_COARSE_MAXITER_2} 
"

echo "Run command is:"
echo ${RUN_COMMAND}
echo `date`
eval ${RUN_COMMAND}
echo `date`
