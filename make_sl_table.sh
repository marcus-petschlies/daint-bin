#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh$/,"",$NF);print $NF}')

path="."
prefix=p2gg
suffix=aff
nsrc=-1
nfile=
output_filename=NA
conf_file=$1
ensemble_name=
stream=" "
with_config_subdir="yes"
with_source_coords=""
with_formatted_conf="no"
skey=""
Conf=()

if [ "X$conf_file" == "X" ]; then
  echo "[$MyName] Warning, no conf_file"
else
  if ! [ -e $conf_file ]; then
    echo "[$MyName] Error, cannot find conf_file"
    exit 2
  fi
  Conf=( $( cat $conf_file ))
fi

source $MyName.in

if [ "X${nfile}X" == "XX" ]
then
  nfile=$nsrc
fi

num_conf=$( echo ${Conf[*]} | wc -w )

cat << EOF
# [$MyName] path               = $path
# [$MyName] prefix             = $prefix
# [$MyName] suffix             = $suffix
# [$MyName] nsrc               = $nsrc
# [$MyName] output_filename    = $output_filename
# [$MyName] conf_file          = $conf_file 
# [$MyName] 
# [$MyName] num_conf           = $num_conf
# [$MyName] 
# [$MyName] with_config_subdir = $with_config_subdir
# [$MyName] 
# [$MyName] with_source_coords = $with_source_coords
# [$MyName] 
# [$MyName] Conf               = ${Conf[*]}
# [$MyName] 
# [$MyName] stream             = $stream
# [$MyName] 
EOF

rm $output_filename
# echo "# [$MyName] (`date`)" > $output_filename
# Conf=($( ls [0-9]*/${prefix}.*.${suffix} 2>/dev/null | awk -F. '{print $2+0}' | sort -un ))

for d in ${Conf[*]}
do
  dfmt=$d
  if [ "X${with_formatted_conf}X" == "XyesX" ]
  then
    dfmt=`printf "%.4d" $d`
  fi
  file_list=
  if [ "X${with_config_subdir}X" == "XyesX" ]; then
    file_list=$(ls $path/$d/${prefix}.${dfmt}*.${suffix}  2>/dev/null )
  else
    file_list=$(ls $path/${prefix}.${dfmt}*.${suffix}  2>/dev/null )
  fi
  file_num=$(echo $file_list | wc -w )
  if [ $file_num -ne $nfile ]; then
    echo "[$MyName] Error for $d, number of files $file_num != number of files $nfile"
    # exit 2
    continue
  fi
  
  for f in $file_list
  do

    sc=()

    if [ "X${with_source_coords}X" == "XtxyzX" ]; then
      sc=($(echo $f | awk -F\/ '{print $NF}' | awk -F. '{gsub(/[txyz]/," ",$3 ); print $3}' | awk '{printf("%d,%d,%d,%d ", $1+0, $2+0, $3+0, $4+0) }' ))
    elif [ "X${with_source_coords}X" == "Xt+sX" ]; then
      sc=($(echo $f | awk -F\/ '{print $NF}' | awk -F. '{sub(/t/," ",$3); sub(/s/," ",$4); printf("%d,%d,%d,%d ", $3+0, $4+0, 0, 0 ) }'))

      k=$( h5ls ${f}/${skey}/t${sc[0]} | awk -F\/ '{sub(/s/,"",$NF);sub(/[\ ]+Group$/,"",$NF); print $NF}' )

      sc[2]=$k
    elif [ "X${with_source_coords}X" == "XtX" ]
    then
      sc=($(echo $f | awk -F\/ '{print $NF}' | awk -F. '{sub(/t/," ",$3); sub(/s/," ",$4); printf("%d,%d,%d,%d ", $3+0, 0, 0, 0) }'))
    elif  [ "X${with_source_coords}X" == "Xh5/tX" ]
    then
      sc=( $( h5ls $f | awk '/^t[0-9]/ {sub(/^t/,"",$1); print }' | sort -un | awk '{printf("%d,%d,%d,%d ", $1,0,0,0) }' ))
    fi

    nsc=$(echo ${sc[*]} | wc -w)
    if [ $nsc -ne $nsrc ]
    then
      echo "[$MyName] Error for $d, wrong number of sources $nsc != $nsrc"
      continue
    fi

    for ((i=0;i<$nsrc;i++))
    do
      p=($(echo ${sc[$i]} | tr ',' ' ' ))
      printf "%s %6d %3d %3d %3d %3d\n" $stream $d ${p[*]} >> $output_filename
    done
  done
done 


# echo "# [$MyName] (`date`)" >>  $output_filename

exit 0
