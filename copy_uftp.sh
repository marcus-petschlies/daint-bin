#!/bin/bash

# uftp env setup for JSC/JUDAC
export UFTP_USER=petschlies1
export UFTP_AUTH_URL=https://uftp.fz-juelich.de:9112/UFTP_Auth/rest/auth/JUDAC
export UFTP_KEY=$HOME/.uftp/id_uftp

remote_dir=/p/scratch/chbn28/hbn289/nf211/cB211a.072.64/configs

echo "# [] (`date`) start copy"

uftp cp --identity $UFTP_KEY $UFTP_AUTH_URL:${remote_dir}/conf.* .

echo "# [] (`date`) all done"

exit 0
