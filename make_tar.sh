#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')
log=${MyName}.`date +%Y_%m_%d_%H_%M`.log
echo "# [$MyName] (`date`)" | tee $log

Conf=(  )

target_dir=

prefix=""
suffix=""

source $MyName.in

cat << EOF | tee -a $log
# [$MyName] target_dir      = $target_dir
# [$MyName] prefix          = $prefix
# [$MyName] suffix          = $suffix
# [$MyName] Conf            = ${Conf[*]}
# [$MyName]

EOF
sleep 1s
echo "# [$MyName] Okay ? (yes)"
read answer
if [ "X$answer" != "Xyes" ]; then
  echo "[$MyName] abort"
  exit 1
else
  echo "# [$MyName] continue"
fi

for g in ${Conf[*]}; do

  gfmt=`printf "%.4d" $g`

  tarfile=${prefix}.${g}.tar

  echo "# [$MyName] (`date`) start $g"
  tar -cf $tarfile ${prefix}.${gfmt}.*.${suffix}
  es=$?
  if [ $es -ne 0 ]; then
    echo "[$MyName] Error from tar for $g"
    exit 1
  fi

  if [ "X$target_dir" != "XNA" ]; then
    mv $tarfile $target_dir/
    es=$?
    if [ $es -ne 0 ]; then
      echo "[$MyName] Error from mv for $g"
      exit 1
    fi
  fi
  echo "# [$MyName] (`date`) end $g"

done | tee -a  $log

echo "# [$MyName] (`date`) all done" | tee -a $log

exit 0
