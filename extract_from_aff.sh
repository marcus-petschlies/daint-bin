#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh$/,"",$NF);print $NF}')

top_level_dir=$PWD
log=${top_level_dir}/$MyName.log

Conf=( )

key_list=( )

prefix=

source ${MyName}.in

cat << EOF > $log
# [$MyName] prefix      = ${prefix}
# [$MyName] key_list    = ${key_list[*]}
# [$MyName]
# [$MyName] Conf        = ${Conf[*]}

EOF

for key in ${key_list[*]}; do

  fnew_prefix=$(echo $key | awk '{sub(/^\//,"",$0);sub(/\/$/,"",$0);gsub(/\//,".",$0);print}')
cat << EOF >> $log
# [$MyName]   key         = $key
# [$MyName]   fnew_prefix = $fnew_prefix

EOF
  for d in ${Conf[*]}; do

    dfmt=`printf "%.4d" $d`

#    cd $top_level_dir/$d

    file_list=($(ls ${prefix}.${dfmt}.*.aff 2>/dev/null))

    for f in ${file_list[*]}; do
      fnew=$(echo $f | awk '{sub(/'"${prefix}"'/,"'"${fnew_prefix}"'",$0);print}' )

      echo "# [$MyName]     $f $fnew" >> $log

      lhpc-aff extract $f $key $fnew $key >> $log 2>&1 

      es=$?
      if [ $es -ne 0 ]; then
        echo "[$MyName] Error from lhpc-aff for file $f, status was $es" >> $log
        exit 1
      fi

    done
  done
done

exit 0
