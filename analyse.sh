#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh$/,"",$NF);print $NF}')
log=$MyName.log
out=$MyName.out
err=$MyName.err

echo "# [$MyName] (`date`) start"

topLevelDir=$PWD

export OMP_NUM_THREADS=4

# module load cray-hdf5/1.10.2.0

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/users/mpetschl/software/hdf5-1.10.4/build-frontend/lib/

input=analyse.input
out=out
err=err

EXECDIR=/project/s1197/mpetschl/software/dev-cpff/build-serial-frontend

ENSEMBLE_NAME="cD211.054.96"

SOURCE_FILE="source_coords.${ENSEMBLE_NAME}.lst"

NCONF=$(awk '{print $1"_"$2}' $SOURCE_FILE | sort -u  | wc -l )
NSRC=$(($(cat $SOURCE_FILE | wc -l) / $NCONF ))

cat << EOF
# [$MyName] NCONF = $NCONF
# [$MyName] NSRC  = $NSRC
EOF

WRITE_DATA="1"

CHARGED_PS=""

OPERATOR_TYPE=1

### valgrind -v --show-reachable=yes --leak-check=full \

# p_orbit_tag=( "0,0,1" )
p_orbit_tag=( "0,0,1" "0,1,1" "1,1,1"  )

num_orbits=$( echo ${p_orbit_tag[*]} | wc -w)
# loop on orbits
for(( orbit=0; orbit < $num_orbits; orbit++ ))
do
    
  cp $input $input.$orbit
  ./make_orbit.sh ${p_orbit_tag[$orbit]} sink_momentum >> $input.$orbit

  out=out.$orbit
  err=err.$orbit
 
  $EXECDIR/p2gg_analyse -f $input.$orbit -O $OPERATOR_TYPE  -N $NCONF -S $NSRC -w  $WRITE_DATA -E $ENSEMBLE_NAME $CHARGED_PS 1>$out 2>$err

  es=$?
  if [ $es -ne 0 ]; then
    echo "[$MyName] Error from prog, status was $es"
    exit 1
  else
    echo "# [$MyName] status 0"
  fi

done  # end of loop on orbit

echo "# [$MyName] (`date`) finished all"
exit 0
