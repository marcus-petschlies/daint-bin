#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')
date_tag=`date +%Y_%m_%d_%H_%M`

log=${MyName}.${date_tag}.log

mis=${MyName}.${date_tag}.missing
fli=${MyName}.${date_tag}.filelist
src=${MyName}.${date_tag}.sourcecoords
smiss=""
echo "# [$MyName] (`date`)" | tee $log

Conf=()

path_to_data=.
p2gg_size=
p2gg_number=
with_source_coords="no"
with_formatted_conf="yes"
prefix="NA"
source_coords_file="NA"
suffix=".aff"
source_coords_select=(1 0 )
stream=""
source_coords_formatted="yes"

source $MyName.in

if [ "X$p2gg_number" == "X" ]; then
  echo "[$MyName] Error, need p2gg_number"
  exit 1
fi

if [ "X$p2gg_size" == "X" ]; then
  echo "[$MyName] Error, need p2gg_size"
  exit 1
fi

cat << EOF | tee -a $log
# [$MyName] path_to_data        = $path_to_data
# [$MyName]
# [$MyName] p2gg_size           = $p2gg_size
# [$MyName]
# [$MyName] p2gg_number         = $p2gg_number
# [$MyName]
# [$MyName] prefix              = $prefix
# [$MyName]
# [$MyName] with_formatted_conf = $with_formatted_conf
# [$MyName] with_source_coords  = $with_source_coords
# [$MyName] source_coords_file  = $source_coords_file
# [$MyName] source_coords_select  = ${source_coords_select[0]}, ${source_coords_select[1]}
# [$MyName]
# [$MyName] Conf                = ${Conf[*]}

EOF
sleep 1s
echo "# [$MyName] Okay ? (yes)"
read answer
if [ "X$answer" != "Xyes" ]; then
  echo "[$MyName] abort"
  exit 1
else
  echo "# [$MyName] continue"
fi


for i in 0; do
  printf "# conf - allok --- number of files - number of files is okay --- filesize is okay\n"
  printf "# =======================================================================================================================================\n\n" >> $log
done >> $log

echo "# [$MyName] `date`" > $fli

echo "# [$MyName] `date`" > $mis 

if [  "X${with_source_coords}X" == "XyesX"  ]; then
  echo "# [$MyName] `date`" > $src
elif [ "X${with_source_coords}X" == "Xt+sX" ]; then
  smiss=${MyName}.${date_tag}.smiss
  echo "# [$MyName] `date`" > $smiss
fi

if [ "X${source_coords_file}X" != "XNAX" ]; then
  if ! [ -e ${source_coords_file} ]; then
    source_coords_file="NA"
    echo "# [$MyName] Warning, reset source_coords_file = $source_coords_file"
  fi
fi

# ********************************************************************
# * loop on configurations
# ********************************************************************
for g in ${Conf[*]}; do

  gfmt=
  if [ "X${with_formatted_conf}X" == "XyesX" ]; then
    gfmt=`printf "%.4d" $g`
  else
    gfmt=$g
  fi

  file_list=()
  # ********************************************************************
  if [ "X${with_source_coords}X" == "XtxyzX" ]; then
  # ********************************************************************

    if [ "X${source_coords_file}X" != "XNAX" ]; then
      source_coords_tags=
      if [ "X${stream}X" == "XX" ]
      then
        if [ "X${source_coords_formatted}X" == "XyesX" ]
        then
          source_coords_tags=($( awk '$1=='$g' {printf("t%.2dx%.2dy%.2dz%.2d\n", $2, $3, $4, $5)}' $source_coords_file | awk '(NR-1)%'${source_coords_select[0]}' == '${source_coords_select[1]}' ' ))
        else
          source_coords_tags=($( awk '$1=='$g' {printf("t%dx%dy%dz%d\n", $2, $3, $4, $5)}' $source_coords_file | awk '(NR-1)%'${source_coords_select[0]}' == '${source_coords_select[1]}' ' ))
        fi
      else
        if [ "X${source_coords_formatted}X" == "XyesX" ]
        then
          source_coords_tags=($( awk '$1=="'$stream'" && $2=='$g' {printf("t%.2dx%.2dy%.2dz%.2d\n", $3, $4, $5, $6)}' $source_coords_file | awk '(NR-1)%'${source_coords_select[0]}' == '${source_coords_select[1]}' ' ))
        else
          source_coords_tags=($( awk '$1=="'$stream'" && $2=='$g' {printf("t%dx%dy%dz%d\n", $3, $4, $5, $6)}' $source_coords_file | awk '(NR-1)%'${source_coords_select[0]}' == '${source_coords_select[1]}' ' ))
        fi
      fi

      # echo "# [$MyName] source_coords_tags = ${source_coords_tags[*]}"
      p2gg_number=$(echo ${source_coords_tags[*]} | wc -w )
      # echo "# [$MyName] p2gg_number(1) = $p2gg_number / ${source_coords_select[0]} - ${source_coords_select[1]}"

      for s in ${source_coords_tags[*]}; do
        f=${path_to_data}/${g}/${prefix}.${gfmt}.${s}${suffix}
        # echo "# [$MyName] f = $f"
        if [ -e $f ]; then 
        # if [ -h $f ]; then 
          file_list=( ${file_list[*]} $f )
        else
          sc=($(echo $f | awk -F\/ '{print $NF}' | awk -F. '{gsub(/[txyz]/," ",$3); print $3}' | awk '{print $1+0, $2+0, $3+0, $4+0}'))
          if [ "X${stream}X" == "XX" ]
          then
            printf "%6d  %s\n" $g  $f >> $mis
            printf "%6d %3d %3d %3d %3d\n" $g ${sc[*]} >> $src
          else
            printf "%s %6d  %s\n" $stream $g  $f >> $mis
            printf "%s %6d %3d %3d %3d %3d\n" $stream $g ${sc[*]} >> $src
          fi
        fi
      done
    else
      file_list=($(ls -rt $path_to_data/$g/${prefix}.${gfmt}.t*x*y*z*[0-9]${suffix} 2>/dev/null | tr ' ' '\n' | head -n $p2gg_number ))
    fi

  # ********************************************************************
  elif [ "X${with_source_coords}X" == "XtX" ]; then
  # ********************************************************************

    if [ "X${source_coords_file}X" != "XNAX" ]; then
      # source_coords_tags=($( awk '$1=='$g' {printf("t%.2dx%.2dy%.2dz%.2d\n", $2, $3, $4, $5)}' $source_coords_file | awk '(NR-1)%'${source_coords_select[0]}' == '${source_coords_select[1]}' ' ))
      source_t_tags=($( awk '$1=="'$stream'" && $2=='$g' {printf("t%d\n", $3)}' $source_coords_file | awk '(NR-1)%'${source_coords_select[0]}' == '${source_coords_select[1]}' ' ))
      source_coords_tags=($( awk '$1=="'$stream'" && $2=='$g' {printf("t%.2dx%.2dy%.2dz%.2d\n", $3, $4, $5, $6)}' $source_coords_file | awk '(NR-1)%'${source_coords_select[0]}' == '${source_coords_select[1]}' ' ))


      #echo "# [$MyName] source_coords_tags = ${source_coords_tags[*]}"
      p2gg_number=$(echo ${source_coords_tags[*]} | wc -w )
      #echo "# [$MyName] p2gg_number(1) = $p2gg_number / ${source_coords_select[0]} - ${source_coords_select[1]}"

      for ((i=0; i<$p2gg_number;i++)); do
        s=${source_t_tags[$i]}
        f=${path_to_data}/${g}/${prefix}.${gfmt}.${s}${suffix}
        #echo "# [$MyName] f = $f"
        if [ -e $f ]; then
        # if [ -h $f ]; then
          file_list=( ${file_list[*]} $f )
        else
          # sc=($(echo $f | awk -F\/ '{print $NF}' | awk -F. '{gsub(/[t]/," ",$3); print $3}' | awk '{print $1+0}'))
          sc=($(echo ${source_coords_tags[$i]} | awk '{gsub(/[txyz]/," ",$0); print $0}' | awk '{print $1+0, $2+0, $3+0, $4+0}'))
          printf "%6d  %s\n" $g  $f >> $mis
          printf "%s%6d %3d %3d %3d %3d\n" "${stream}" $g ${sc[*]} >> $src
        fi
      done
    else
      # echo "# [$MyName] this is the branch"
      # ls -rt $path_to_data/$g/${prefix}.${gfmt}.t*[0-9]${suffix} 2>/dev/null 
      file_list=($(ls -rt $path_to_data/$g/${prefix}.${gfmt}.t*[0-9]${suffix} 2>/dev/null | tr ' ' '\n' | head -n $p2gg_number ))
    fi

  # ********************************************************************
  elif [ "X${with_source_coords}X" == "Xt+sX" ]; then
  # ********************************************************************

    if [ "X${source_coords_file}X" == "XNAX" ]; then
      # echo "# [$MyName] this is the branch"
    
      file_list=($(ls -rt $path_to_data/$g/${prefix}.${gfmt}.t*.s*${suffix} ))
      # echo "# $g ${file_list[*]}"
      # file_list=($( find $path_to_data/$g/ -type f | awk '/'${prefix}'\.'${gfmt}'\.t[0-9]+/ && /\.s[0-9]+'$suffix'/ {print}' ))

      nf=$( echo ${file_list[*]} | wc -w )
      if [ $nf -gt $p2gg_number ]; then
        # echo "[$MyName] Error, too many files match pattern, $nf > $p2gg_number"
        # exit 1
        echo "# [$MyName] Warning, too many files match pattern, $nf > $p2gg_number"
      fi

#      file_list=()
#      for((i=0;i<$p2gg_number;i++))
#      do
#        # f=($( ls -rt $path_to_data/$g/${prefix}.${gfmt}.t+([0-9]).s${i}${suffix} 2>/dev/null | tr ' ' '\n'  ))
#        f=($( find $path_to_data/$g/ -type f | awk '/'${prefix}'\.'${gfmt}'\.t+[0-9]/ && /\.s'${i}''$suffix'/ {print}' ))
#
#        nf=$( echo ${f[*]} | wc -w )
#        if [ $nf -gt 1 ]; then
#          #echo "[$MyName] Error, too many files for s=$i, ${f[*]}"
#          #exit 1
#          echo "[$MyName] Warning, too many files for s=$i, ${f[*]}"
#        elif [ ${nf} == 0 ]; then
#          printf "%6d %3d\n" $g $i >> $smiss
#        else
#          file_list=( ${file_list[*]} ${f[*]} )
#        fi
#      done
    fi  # of if source_coords_file = NA

  # ********************************************************************
  else  # without any format of source coords 
  # ********************************************************************
    if [ "X${with_config_dir}X" == "XyesX" ]
    then
      if [ "X${with_formatted_conf}X" == "XyesX" ]
      then
        file_list=($(ls -rt $path_to_data/$g/${prefix}.${gfmt}${suffix} 2>/dev/null | tr ' ' '\n' | head -n $p2gg_number ))
      else
        file_list=($(ls -rt $path_to_data/$g/${prefix}.${g}${suffix} 2>/dev/null | tr ' ' '\n' | head -n $p2gg_number ))
      fi
    else
      file_list=($(ls -rt $path_to_data/${prefix}.${gfmt}${suffix} 2>/dev/null | tr ' ' '\n' | head -n $p2gg_number ))
    fi
  fi

  # ********************************************************************
  # * check file number
  # ********************************************************************
  nf=$(echo ${file_list[*]} | wc -w)
  # echo "# [$MyName] p2gg_number = $p2gg_number"
  # if [ $nf -ne $p2gg_number ]
  if [ $nf -lt $p2gg_number ]
  then
    file_number_ok=0
  else
    file_number_ok=1
  fi

  # ********************************************************************
  # * init file size okay
  # ********************************************************************
  # for(( i=0; i<$p2gg_number; i++))
  file_size_ok=()
  for(( i=0; i<$nf; i++))
  do
    file_size_ok[$i]=0
  done

  # ********************************************************************
  # * check individual file sizes
  # ********************************************************************
  for((i=0; i<$nf; i++))
  do
    f=${file_list[$i]}
    # echo "f = $f"
    s=$(stat -c %s $f)
    if [ $s -lt  $p2gg_size ]; then
      file_size_ok[$i]=0
      if [  "X${with_source_coords}X" == "XtxyzX"  ]; then
        sc=($(echo $f | awk -F\/ '{print $NF}' | awk -F. '{gsub(/[txyz]/," ",$3); print $3}' | awk '{print $1+0, $2+0, $3+0, $4+0}'))
        printf "%6d  %s\n" $g  $f >> $mis
        if [ "X${stream}X" == "XX" ]
        then
          printf "%6d %3d %3d %3d %3d\n" $g ${sc[*]} >> $src
        else
          printf "%s %6d %3d %3d %3d %3d\n" $stream $g ${sc[*]} >> $src
        fi
      elif [ "X${with_source_coords}X" == "Xt+sX" ]; then
        echo $f | awk -F. '{printf ("%6d %3d  *\n", '$g', $(NF-1))}' >> $smiss
      else
        echo $f >> $mis
      fi
    else
      file_size_ok[$i]=1
      echo $f >> $fli
    fi
  done
  allok=$file_number_ok
  for((i=0; i<$nf; i++)); do
    allok=$(( $allok && ${file_size_ok[$i]} ))
  done

  printf "%6d %d     %3d %d   " $g $allok $nf $file_number_ok
  # for(( i=0; i<$p2gg_number; i++))
  for(( i=0; i<$nf; i++))
  do
    printf "%d " ${file_size_ok[$i]}
  done
  printf "\n"

#  printf "# ---------------------------------------------------------------------------------------------------------------------------------------\n"
done >> $log 2>&1 # 2>&1 |   tee -a $log  # 

  printf "\n# =======================================================================================================================================\n\n" >> $log

echo "# [$MyName] (`date`) all done" | tee -a $log
exit 0
