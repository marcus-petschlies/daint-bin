#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')
top_level_dir=$PWD

Conf=( )

prefix="NA"
suffix="NA"
ens_name="NA"

nf_ref=0

if [ -e ${MyName}.in ]; then
  echo "# [$MyName] source ${MyName}.in"
  source ${MyName}.in
fi

log=$top_level_dir/${MyName}.log

if [ "X$project_dir" == "X" ]; then
  project_dir=/project/s849/mpetschl/data/${prefix}/${ens_name}
fi


cat << EOF | tee -a $log
# [$MyName] (`date`)
# [$MyName] top_level_dir = $top_level_dir
# [$MyName] prefix        = $prefix
# [$MyName] suffix        = $suffix
# [$MyName] ens_name      = $ens_name
# [$MyName] project_dir   = $project_dir
# [$MyName] 
# [$MyName] num conf      = $(( $(echo ${Conf[*]} | wc -w) ))
# [$MyName] nf_ref        = $nf_ref
# [$MyName] 

EOF

for d in ${Conf[*]}; do

  echo "# [$MyName] (`date`) start $d"

  dfmt=`printf "%.4d" $d`

  file_list=( $(find ./$d/ -type f -name ${prefix}.${dfmt}.\*.${suffix} )  )
  nf=$(echo ${file_list[*]} | wc -w)
  if [ $nf -ne $nf_ref ]; then
    echo "[$MyName] Error for $d; $nf != $nf_ref"
    continue
  else
    echo "# [$MyName] $d $nf"
  fi

  tar_file=${prefix}_${dfmt}_${suffix}.tar
  tar -cf ${tar_file} ${file_list[*]}
  if [ $? -ne 0 ]; then
    echo "[$MyName] Error from tar for $tar_file"
    rm -v $tar_file
    continue
  fi
    
  mv $tar_file $project_dir/ 

  echo "# [$MyName] (`date`) end $d"
  echo

done 2>&1 | tee -a $log

exit 0
