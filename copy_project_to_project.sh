#!/bin/bash -l
#SBATCH --job-name="p2p"
#SBATCH --time=24:00:00
#SBATCH --partition=xfer
#SBATCH --hint=nomultithread

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

# module unload xalt

source_dir=/project/s982/mpetschl/data
target_dir=/project/s1045/mpetschl/data

echo "# [$SLURM_JOB_ID] (`date`) start"

srun rsync -auvz --remove-source-files ${source_dir}/p2gg_local ${target_dir}/

es=$?
if [ $es -ne 0 ]; then
  echo "[$SLURM_JOB_ID] Error from srun rsync, status was $es "
  exit 1
fi
 
echo "# [$SLURM_JOB_ID] (`date`) end"

exit 0
