#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh$/,"",$NF);print $NF}')

p=($( echo $1 | tr ',' ' '))

name=$2

if [ "X${name}X" == "XX" ]; then
  echo "[$MyName] Error, need name for momentum list"
  exit 1
fi

if [ "X${p[2]}" == "X" ]; then
  echo "[$MyName] Error, need p"
  exit 1
fi

if [ "X${p[1]}" == "X" ]; then
  echo "[$MyName] Error, need p"
  exit 1
fi

if [ "X${p[0]}" == "X" ]; then
  echo "[$MyName] Error, need p"
  exit 1
fi

#p[0]=$( echo ${p[0]} | tr '-' ' ' )
#p[1]=$( echo ${p[1]} | tr '-' ' ' )
#p[2]=$( echo ${p[2]} | tr '-' ' ' )
p=($( echo ${p[*]} | awk '{gsub(/-/,"",$0);print}' ))

if [ ${p[0]} -gt ${p[1]} ]; then
  t=${p[1]}
  p[1]=${p[0]}
  p[0]=$t
fi

if [ ${p[1]} -gt ${p[2]} ]; then
  t=${p[2]}
  p[2]=${p[1]}
  p[1]=$t
fi

if [ ${p[0]} -gt ${p[1]} ]; then
  t=${p[1]}
  p[1]=${p[0]}
  p[0]=$t
fi

### echo "# [$MyName] p = ${p[0]}, ${p[1]}, ${p[2]}"

for s0 in 1 -1; do
for s1 in 1 -1; do
for s2 in 1 -1; do

  q=( $(( ${p[0]}*$s0 )) $(( ${p[1]}*$s1 )) $(( ${p[2]}*$s2 )))
  echo ${q[*]}

  q=( $(( ${p[1]}*$s0 )) $(( ${p[2]}*$s1 )) $(( ${p[0]}*$s2 )))
  echo ${q[*]}

  q=( $(( ${p[2]}*$s0 )) $(( ${p[0]}*$s1 )) $(( ${p[1]}*$s2 )))
  echo ${q[*]}

  q=( $(( ${p[0]}*$s0 )) $(( ${p[2]}*$s1 )) $(( ${p[1]}*$s2 )))
  echo ${q[*]}

  q=( $(( ${p[2]}*$s0 )) $(( ${p[1]}*$s1 )) $(( ${p[0]}*$s2 )))
  echo ${q[*]}

  q=( $(( ${p[1]}*$s0 )) $(( ${p[0]}*$s1 )) $(( ${p[2]}*$s2 )))
  echo ${q[*]}

done
done
done | sort -u > .$MyName.tmp

awk 'BEGIN{ printf("%s = %d,%d,%d\n", "'${name}'", '${p[0]}', '${p[1]}', '${p[2]}')}
     $1==('${p[0]}'+0) && $2==('${p[1]}'+0) && $3==('${p[2]}'+0) {next}
     {printf("%s = %d,%d,%d\n", "'${name}'", $1, $2, $3); next}'  .$MyName.tmp

exit 0
