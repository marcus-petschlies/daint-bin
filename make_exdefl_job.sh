#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')

echo "# [$MyName] (`date`)"

Conf=( )

work_path=
cvc_exec_dir=

source_coords_filename=
source_coords_select=( 1 0 )
n_coherent_source=1

myemail=

cvc_exec_name=
cvc_exec_options=

with_submit=no
TT=
LL=
ensemble_name=

invcon_production=yes

walltime_prod=12:00:00
tasks_per_core=1
nodes_per_call=4
tasks_per_node=1
cpus_per_task=12
partition=normal
gpus_per_node=1
account=s849


run_prefix=""

QUDA_ENABLE_GDR=0
QUDA_ENABLE_P2P=3
QUDA_COMMIT_HASH="8e9b6a32ca1b8e4397e1f18f0800e76a9a9416c6"
QUDA_ENABLE_DEVICE_MEMORY_POOL=0
QUDA_ENABLE_DSLASH_COARSE_POLICY=0

answer_wait_time=1

job_mask=
cvc_input_mask=
tmLQCD_input_mask=


if [ $# -eq 0 ]; then
  echo -e "# [$MyName] WARNING: no command line arguments provided, keeping default values\n"
else
  while [ "$1" ]; do
    case "$1" in
      "-x") with_submit=$2; shift 2;;
      "-e") ensemble_name=$2; shift 2;;
      *) exit 1;;
    esac
  done
fi

if ! [ -e ${MyName}.in ]; then
  echo "[$MyName] Error, could not find ${MyName}.in"
  exit 1
fi
source ${MyName}.in

if [ "X$run_prefix" == "X" ]; then
  echo "[$MyName] Error, need a run prefix"
  exit 1
fi

if [ "X$TT" == "X" ] || [ "X$LL" == "X" ] ; then
  echo "# [$MyName] Error, need L and T"
  exit 1
fi

if [ "X$ensemble_name" == "X" ] ; then
  echo "# [$MyName] Error, need ensemble name"
  exit 1
fi

if [ "X$source_coords_filename" == "X" ] ; then
  echo "# [$MyName] Error, need source coords filename"
  exit 1
fi

if [ "X$nodes_per_call" == "X" ] || [ "X$tasks_per_node" == "X" ] || [ "X$cpus_per_task" == "X" ] ; then
  echo "# [$MyName] Error, number of nodes_per_call, tasks_per_node, cpus_per_task"
  exit 1
fi

if [ "X$cvc_exec_dir" == "X" ] || [ "X$work_path" == "X" ]; then
  echo "# [$MyName] Error, need exec_dir and work_path"
  exit 1
fi

if [ "X$cvc_exec_name" == "X" ]; then
  echo "# [$MyName] Error, need cvc_exec_name"
  exit 1
fi

if [ "X$partition" == "X" ]; then
  echo "# [$MyName] Error, need partition"
  exit 1
fi

if [ "X$myemail" == "X" ]; then
  echo "# [$MyName] Error, need myemail"
  exit 1
fi

nconf=$(echo ${Conf[*]} | wc -w)

if [ $tasks_per_node -ne $gpus_per_node ]; then
  echo "# [$MyName] resetting tasks_per_node <- gpus_per_node"
  tasks_per_node=$gpus_per_node
fi

JOB_LOG=$work_path/JOB_LOG


cat << EOF
###########################################################
###########################################################
##                                                       ##
## [$MyName] did you check the input files?   ##
##                                                       ##
###########################################################
###########################################################
#
# [$MyName] with_submit            = $with_submit
# [$MyName] ensemble name          = $ensemble_name
# [$MyName] work_path              = $work_path
# [$MyName]
# [$MyName] cvc_exec_dir           = $cvc_exec_dir
# [$MyName] cvc_exec_name          = $cvc_exec_name
# [$MyName] cvc_exec_options       = $cvc_exec_options
# [$MyName]
# [$MyName] T                      = $TT
# [$MyName] L                      = $LL
# [$MyName]
# [$MyName] source coords file     = $source_coords_filename
# [$MyName] source coords select   = ${source_coords_select[0]}, ${source_coords_select[1]}
# [$MyName]
# [$MyName] job mask               = $job_mask
# [$MyName]
# [$MyName] input mask             = $cvc_input_mask
# [$MyName] tmLQCD_input_mask      = $tmLQCD_input_mask
# [$MyName]
# [$MyName] Conf                   = ${Conf[*]}
# [$MyName]
# [$MyName] walltime_prod          = $walltime_prod
# [$MyName]
# [$MyName] nodes_per_call         = $nodes_per_call
# [$MyName] tasks_per_node         = $tasks_per_node
# [$MyName] cpus_per_task          = $cpus_per_task
# [$MyName]
# [$MyName] gpus_per_node          = $gpus_per_node
# [$MyName] quda_resource_path     = $quda_resource_path
# [$MyName]
# [$MyName] myemail                = $myemail
# [$MyName]
# [$MyName] account                = $account


EOF

sleep ${answer_wait_time}s
echo "# [$MyName] Okay ? (yes)"
read answer
if [ "X$answer" != "Xyes" ]; then
  echo "[$MyName] abort"
  exit 1
else
  echo "# [$MyName] continue"
fi

###########################################################
# inversion+contraction job
###########################################################
if [ "X$invcon_production" == "Xyes" ]; then

  for g in ${Conf[*]}; do

    date_tag=`date +%Y_%m_%d_%H_%M_%S`

    gfmt=$(printf "%.4d" $g)

    WDIR=$work_path/$g
    mkdir -p $WDIR 
    cd $WDIR

    job_tag=${g}.${date_tag}
    job=${run_prefix}.${job_tag}.sh

    cvc_input=${run_prefix}.${job_tag}.input
    tmLQCD_input=invert.input
   
cat << EOF
# [$MyName] gauge config           = $g
# [$MyName] work_dir               = $PWD
# [$MyName]
# [$MyName] date_tag               = $date_tag
# [$MyName] job_tag                = $job_tag
# [$MyName] job                    = $job
# [$MyName] cvc_input              = $cvc_input
# [$MyName] tmLQCD_input           = $tmLQCD_input

EOF

    cat $job_mask | awk '
      /^TRAJ=/ {print "TRAJ='$g'"; next}
      /^#SBATCH --job-name[\ =]/  { print "#SBATCH --job-name='${ensemble_name}'_prod_'${job_tag}'"; next }
      /^MYLOCALDIR=/              { print "MYLOCALDIR='${work_path}'"; next}
      /^CONF_DIR=/                { print "CONF_DIR='${path_to_conf}'"; next}
      /^L=/                       { print "L='${LL}'"; next}
      /^T=/                       { print "T='${TT}'"; next}
      /^NUM_THREADS=/             { print "NUM_THREADS='${cpus_per_task}'"; next}
      /^MYQUDADIR=/               { print "MYQUDADIR='${quda_resource_path}'"; next}
      /^QUDA_BIN=/                { print "QUDA_BIN='${quda_exec_dir}'/'${quda_exec_name}'"; next}
      /^#SBATCH --mail-user[\ =]/ { print "#SBATCH --mail-user='${myemail}'"; next}
      /^#SBATCH --nodes[\ =]/     { print "#SBATCH --nodes='${nodes_per_call}'"; next}
      /^#SBATCH --ntasks[\ =]/    { print "#SBATCH --ntasks='${nodes_per_call}'"; next }
      /^#SBATCH --ntasks-per-cor[\ =]/  { print "#SBATCH --ntasks-per-core='${tasks_per_core}'"; next}
      /^#SBATCH --ntasks-per-node[\ =]/ { print "#SBATCH --ntasks-per-node='${tasks_per_node}'"; next}
      /^#SBATCH --cpus-per-task[\ =]/   { print "#SBATCH --cpus-per-task='${cpus_per_task}'"; next}
      /^#SBATCH --time[\ =]/      { print "#SBATCH --time='${walltime_prod}'"; next}
      /^#SBATCH --partition[\ =]/ { print "#SBATCH --partition='${partition}'"; next}
      /^#SBATCH --account[\ =]/   { print "#SBATCH --account='${account}'"; next}
      /^out=/                     { print "out='${run_prefix}'.out.'${date_tag}'.$SLURM_JOB_ID"; next }
      /^err=/                     { print "err='${run_prefix}'.err.'${date_tag}'.$SLURM_JOB_ID"; next }
      /^cvc_exec_dir=/            { print "cvc_exec_dir='${cvc_exec_dir}'"; next}
      /^cvc_exec_name=/           { print "cvc_exec_name='${cvc_exec_name}'"; next}
      /^cvc_input=/               { print "cvc_input='${cvc_input}'"; next}
      {print; next}' > $job


    source_coords_list=($(awk '
        $1=='$g' && $2<'$TT'/'$n_coherent_source' {
          printf("%d,%d,%d,%d\n", $2,$3,$4,$5)
        }' $source_coords_filename | awk '(NR-1) % '${source_coords_select[0]}' == '${source_coords_select[1]}' '))
    source_coords_number=$(echo ${source_coords_list[*]} | wc -w)
    echo "# [$MyName] source coords number = $source_coords_number"

    # cvc input file
    cat $cvc_input_mask | awk '
      /^Nconf[\ =]/           { print "Nconf = '${g}'"; next}
      /^seed[\ =]/            { print "seed = "(1000000+'${g}'); next}
      /^num_threads[\ =]/     { print "num_threads = '${cpus_per_task}'"; next}
      /^gaugefilename_prefix[\ =]/ {print "gaugefilename_prefix = '${path_to_conf}'/conf"; next }
      {print; next}
      END{printf("\n\n")}' > $WDIR/$cvc_input

    # append source coords list
    for (( isrc=0; isrc<$source_coords_number; isrc++)); do
      printf "source_coords = %s\n" ${source_coords_list[$isrc]}
    done >> $WDIR/$cvc_input

    # submit the job
    if [ "X$with_submit" == "Xyes" ]; then
      echo "# [$MyName] (`date`) $job_tag " 2>&1 | tee -a $JOB_LOG
      sbatch $job 2>&1 | tee -a $JOB_LOG
    fi

    sleep 1s
  done  # end of loop on configurations

fi  # of if invcon job

echo "# [$MyName] (`date`)"
exit 0
