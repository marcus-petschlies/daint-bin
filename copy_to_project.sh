#!/bin/bash -l
#SBATCH --job-name="transfer"
#SBATCH --time=24:00:00
#SBATCH --partition=xfer
#SBATCH --hint=nomultithread
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')
date_tag=`date +%Y_%m_%d_%H_%M`

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

partition=
scratch_dir=
project_dir=
source_prefix=
target_prefix=
suffix=
midfix=

Conf=( )
tag=""

log=

source ${MyName}.in

if [ "X${partition}X" == "XxferX" ]; then
  module unload xalt

  tag=$SLURM_JOB_ID
  log=${MyName}.${SLURM_JOB_ID}.log
else
  tag=$MyName
  log=${MyName}.${date_tag}.log
fi



copy_command=""
if [ "X${partition}X" == "XxferX" ]; then
  copy_command="srun rsync -auv"
elif [ "X${partition}X" == "XfrontendX" ]; then
#  copy_command="rsync -auv"
  copy_command="cp -av"
fi

for g in ${Conf[*]}; do

  gfmt=`printf "%.4d" $g`

  echo "# [$tag] (`date`) start $g"
  cd ${scratch_dir}/${g}/

  file_list=""
  if [ "X${midfix}X" == "XNAX" ]; then
    file_list=$( ls ${source_prefix}.${gfmt}.${suffix} 2>/dev/null )

    # ${copy_command}  ${scratch_dir}/${g}/${source_prefix}.${gfmt}.${suffix} ${project_dir}/${target_prefix}.${gfmt}.${suffix}
  else
    file_list=$( ls ${source_prefix}.${gfmt}.${midfix}.${suffix} 2>/dev/null )
  fi

  for f in $file_list; do
    fout=$( echo $f | awk '{sub(/'$source_prefix'/, "'$target_prefix'",$0);print}')

    ${copy_command}  $f ${project_dir}/${fout}
  done
  es=$?
  if [ $es -ne 0 ]; then
    echo "[$tag] Error from srun rsync, status was $es "
    exit 1
  fi
 
  echo "# [$tag] (`date`) end $g"
done >> $log 2>&1

exit 0
560
564
568
572
576
580
584
588
592
608
616
620
624
628
632
640
644
656
660
664
668
676
680
684
692
696
700
704
708
712
716
720
724
728
732
736
740
744
748
752
756
760
764
768
772
776
780
784
788
792
796
800
804
808
812
816
820
824
828
832
836
