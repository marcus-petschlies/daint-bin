#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')
top_level_dir=$PWD
date_tag=`date +%Y_%m_%d_%H_%M_%S`
log=${top_level_dir}/${MyName}.${date_tag}.log
stream=
flavor=
mu=

archive_dir=

prefix_old=p2gg_local
prefix_new=p2gg_twop_local

source_coords_file=
source_coords_select=( 1 0 )

Conf=( )

answer_wait_time=1

source ${MyName}.in

if [ "X${flavor}X" != "XlightX" ]; then
  if [ "X${mu}X" != "XX" ]; then
    prefix_new=p2gg_twop_local_${mu}
  else
    echo "[$MyName] Error, need mu value" | tee -a $log
    exit 1
  fi
fi

cat << EOF | tee -a $log
# [$MyName] top_level_dir        = $top_level_dir
# [$MyName] 
# [$MyName] stream               = $stream
# [$MyName] flavor               = $flavor
# [$MyName] mu                   = $mu
# [$MyName] 
# [$MyName] archive_dir          = $archive_dir
# [$MyName] 
# [$MyName] prefix_old           = $prefix_old
# [$MyName] prefix_new           = $prefix_new
# [$MyName] 
# [$MyName] source_coords_file   = $source_coords_file
# [$MyName] source_coords_select = ${source_coords_select[*]}
# [$MyName] 
# [$MyName] Conf                 = ${Conf[*]}
# [$MyName] 
EOF


sleep ${answer_wait_time}s
echo "# [$MyName] Okay ? (yes)"
read answer
if [ "X$answer" != "Xyes" ]; then
  echo "[$MyName] abort"
  exit 1
else
  echo "# [$MyName] continue"
fi


mkdir -p $archive_dir || exit 1

for d in ${Conf[*]}; do
 
  echo "# [$MyName] (`date`) start $d" | tee -a $log

  dfmt=`printf "%.4d" $d`

  cd ${top_level_dir}/$d

  source_coords_list=($(awk '$1=='$d' {printf("t%.2dx%.2dy%.2dz%.2d\n", $2, $3, $4, $5) }' $source_coords_file | awk '(NR-1) % '${source_coords_select[0]}' == '${source_coords_select[1]}' ' ))

  file_list=( )
  for s in ${source_coords_list[*]}; do
    file_list=( ${file_list[*]} ${prefix_old}.${dfmt}.${s}.aff )
  done

  for f in ${file_list[*]}; do
    if ! [ -e $f ]; then
      echo "[$MyName] Error, cannot find $f" | tee -a $log
      exit 1
    fi 
    fnew=$( echo $f | awk '{sub(/'${prefix_old}'/,"'${prefix_new}'");print}')
    echo "# [$MyName] $f ---> $fnew " | tee -a $log
    cp -v $f ${archive_dir}/$fnew 2>&1 | tee -a $log
    # mv -v $f ${archive_dir}/$fnew
  done

  echo "# [$MyName] (`date`) end   $d" | tee -a $log
done

exit 0
